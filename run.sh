#!/bin/sh
rm *.text
rm target/scala-2.11/XMPPServerLoadTest-assembly-1.0.jar
sbt assembly
java -server -XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+CMSClassUnloadingEnabled -XX:+CMSPermGenSweepingEnabled -jar target/scala-2.11/XMPPServerLoadTest-assembly-1.0.jar