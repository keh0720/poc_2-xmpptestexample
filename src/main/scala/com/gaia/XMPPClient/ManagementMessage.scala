package com.gaia.XMPPClient

import akka.actor.{Actor, ActorRef}

import scala.collection.mutable.{ListBuffer, Map}

/**
 * Created by ktz on 15. 7. 20.
 */
object ManagementMessage {
  trait XMPPAction
  case class RequestLogInAll() extends XMPPAction
  case class RequestLogOutAll() extends XMPPAction
  case class RequestDeleteAccountAll() extends XMPPAction
  case class RequestCreateAccountAll() extends XMPPAction
  case class RequestToLaunchClient(mServerAddress : String, mHostAddress : String, nOfClient : Int, mID : String, mPW : String) extends XMPPAction
  object RequestToLaunchClient{
    def apply(mServerAddress : String, mHostAddress : String, nOfClient : Int, mPW : String) = new RequestToLaunchClient(mServerAddress, mHostAddress, nOfClient, "", mPW)
  }
  case class SlaveLaunched(mIPAddress : String) extends XMPPAction
  case class MasterConnected() extends XMPPAction
  case class RequestAddFriendEachOther() extends XMPPAction
  case class RequestReLaunchClient() extends XMPPAction

  case class ActorStatic(var connected : Boolean, var reconnecting : Boolean, var nOfReconnect : Int, var nOfFailure : Int)
  case class XMPPErrorStatic(exception : Exception, var nOfError : Int)

  trait SlaveReply
  case class SlaveError(e : Exception) extends SlaveReply
  case class SlaveState(address : String,  nOfSuccess : Int, nOfAuth : Int, nOfReconnecting : Int, nOfReconnect : Int, nOfFailure : Int) extends SlaveReply
  case class MsgEnvelope(from : ActorRef, xmppAction: XMPPAction)
  case class RequestSlaveState() extends XMPPAction
}
