package com.gaia.XMPPClient

import java.net.{NetworkInterface}

import akka.actor._
import com.gaia.XMPPClient.ManagementMessage._
import com.gaia.XMPPClient.Result.{DummyStaticManager, StaticDatabaseActor, StaticManager}
import com.gaia.XMPPClient.XMPPActor.{XMPPClientParentActor, XMPPClientActor, XMPPClientManagerBus}
import com.typesafe.config.ConfigFactory
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._

/**
 * Created by ktz on 15. 7. 20.
 */

trait TestMode
case class NormalMode() extends TestMode
case class BruteMode() extends TestMode
case class ParallelStartMode() extends TestMode
case class RandomStartMode() extends TestMode

case class ActorConfig(testMode : TestMode, packetReplyTimeOut : Long, retryTimeOut : Int, retryTimeMin : Int, logIngSeqTime : Int, clientBunch : Int, bosh : Boolean)

class ActorClient(val mHostAddress : String, val mPort : Int, val mHostActorName : String) extends Actor with ActorLogging{
  case class TryConnectAgain()

  val configFactory = ConfigFactory.load("application")
  val testMode = configFactory.getString("test.testMode") match {
    case "Normal" =>
      NormalMode()
    case "Brute" =>
      BruteMode()
    case "ParallelStart" =>
      ParallelStartMode()
    case "RandomStart" =>
      RandomStartMode()
  }
  val config = ActorConfig(testMode, configFactory.getLong("test.interval.packet-reply"), configFactory.getInt("test.interval.retry"), configFactory.getInt("test.interval.retry-min"),
    configFactory.getInt("test.interval.login"), configFactory.getInt("test.xmpp-client.nOfClientBunch"), configFactory.getBoolean("test.bosh"))

  def this(hostAddress : String, hostActorName : String) = this(hostAddress, 2552, hostActorName)
  implicit val timeout = akka.util.Timeout(10 second)
  var masterActor : ActorRef = Actor.noSender
  val mXMPPClientList : ListBuffer[ActorRef] = ListBuffer.empty
  var nOfXMPPClient : Int = 0
  val mXMPPClientManagerBus = new XMPPClientManagerBus

  import context.dispatcher
  var mCancelable = context.system.scheduler.schedule(0 millisecond, 5 second, self, TryConnectAgain())
  val myAddress = GetMyIPAddress.find(_.contains(configFactory.getString("akka.myip-startswith")))

  var mStaticManager : ActorRef = Actor.noSender

  def receive = {
    case TryConnectAgain() =>
      myAddress match {
        case Some(properString) =>
          context.actorSelection(s"akka.tcp://$mHostAddress:$mPort/user/$mHostActorName") ! SlaveLaunched(properString)
        case None =>
          val errorMsg = "Cannot Find Proper IP Address"
          log.error(errorMsg)
          context.actorSelection(s"akka.tcp://$mHostAddress:$mPort/user/$mHostActorName") ! SlaveError(new Exception(errorMsg))
      }
    case MsgEnvelope(from, xmppAction) => xmppAction match {
      case MasterConnected() =>
        mCancelable.cancel()
        masterActor = sender()
        context watch masterActor
        context become ReadyToReceive
    }
  }

  def ReadyToReceive : Receive = {
    case MsgEnvelope(from, xmppAction) => xmppAction match {
      case RequestToLaunchClient(mServerAddress, mHostAddress, nOfClient, mID, mPW) =>
        mStaticManager = if(configFactory.getBoolean("mysql.enable"))context.system.actorOf(StaticDatabaseActor(mID)) else context.actorOf(DummyStaticManager.props)
        nOfXMPPClient = nOfClient
        if(config.bosh && configFactory.getBoolean("test.self-signed")){
          InstallCert.makeCert(mServerAddress, 80)
          System.setProperty("javax.net.ssl.trustStore", "jssecacerts")
        }

        val nOfParent = if(nOfClient % config.clientBunch != 0) (nOfClient / config.clientBunch) + 1 else nOfClient / config.clientBunch

        (0 until nOfParent) foreach {i=>
          context.system.actorOf(XMPPClientParentActor(mStaticManager, mXMPPClientManagerBus, config.clientBunch, i, mServerAddress, mHostAddress, mID, mPW, config), s"ParentActor_$i")
        }

        log.info("Client Launched")
      case RequestSlaveState() =>
        log.info("Ask Slave State")
        mStaticManager ! RequestSlaveState()
      case xmppAction : XMPPAction =>
        mXMPPClientManagerBus publish MsgEnvelope(self, xmppAction)
    }
    case Terminated(terminatedActor) =>
      if(terminatedActor == masterActor) {
        val reconnectInterval = configFactory.getInt("akka.reconnect-interval")
        context unwatch masterActor
        masterActor = Actor.noSender
        context become receive
        mCancelable = context.system.scheduler.schedule(reconnectInterval millisecond, reconnectInterval millisecond, self, TryConnectAgain())
      }
    case SlaveState(address, nOfConnect, nOfAuth, nOfReconnecting, nOfReconnect, nOfFailure) =>
      masterActor ! SlaveState(address, nOfConnect, nOfAuth, nOfReconnecting, nOfReconnect, nOfFailure)
    case SlaveError(e) =>
      masterActor ! SlaveError(e)
  }

  def GetMyIPAddress : List[String] ={
    import scala.collection.JavaConversions._
    NetworkInterface.getNetworkInterfaces.flatMap(networkInterface =>
      networkInterface.getInetAddresses.map(iNetAddress =>
        iNetAddress.getHostAddress
      )
    ).toList
  }
}