package com.gaia.XMPPClient.Result

import slick.lifted.{ProvenShape, Tag}
import slick.driver.MySQLDriver.api._

/**
 * Created by ktz on 15. 9. 16.
 */
class ConnectionState(tag: Tag) extends Table[(Int, String, String)](tag, "ConnectionState"){
  def ClientID = column[Int]("ClientID")
  def msgKind = column[String]("Message", O.SqlType("varchar(100)"))
  def DisconnectedDate = column[String]("DateTime", O.SqlType("varchar(100)"))
  override def * : ProvenShape[(Int, String, String)] = (ClientID, msgKind, DisconnectedDate)
}
