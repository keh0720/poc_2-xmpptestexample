package com.gaia.XMPPClient.Result

import slick.lifted.{Tag}
import slick.driver.MySQLDriver.api._

/**
 * Created by ktz on 15. 9. 9.
 */
class InstanceState(tag : Tag) extends Table[(String, Int, Int, Int, Int, Int, Long)](tag, s"SlaveState"){
  def InstanceID = column[String]("Client_ID", O.PrimaryKey, O.SqlType("varchar(100)"))
  def nOfConnect = column[Int]("nOfConnected")
  def nOfAuth = column[Int]("nOfAuth")
  def nOfReconnecting = column[Int]("nOfConnecting")
  def nOfReconnected = column[Int]("nOfReconnected")
  def nOfFailure = column[Int]("nOfFailure")
  def updatedTime = column[Long]("UpdatedTime")
  def * = (InstanceID, nOfConnect, nOfAuth, nOfReconnecting, nOfReconnected, nOfFailure, updatedTime)
}