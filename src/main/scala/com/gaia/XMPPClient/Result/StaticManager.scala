package com.gaia.XMPPClient.Result

import java.nio.channels.AlreadyConnectedException

import akka.actor.{Props, Actor}
import com.gaia.XMPPClient.ManagementMessage._
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper.{ConnectionAuthenticated, ConnectionClosedOnError, ConnectionReconnectionSuccessful, ConnectionConnected}
import org.jivesoftware.smack.SmackException.{NoResponseException, NotConnectedException, ConnectionException}
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.sasl.SASLErrorException

import scala.collection.mutable.ListBuffer

/**
 * Created by ktz on 15. 9. 2.
 */

trait StaticManager {
  import scala.collection.mutable.Map
  var mXMPPClientStatic : Map[String, ActorStatic] = Map.empty
  var nOfSuccess : ListBuffer[String] = ListBuffer.empty
  var nOfReconnecting : ListBuffer[String] = ListBuffer.empty
  var nOfAuthenticated : ListBuffer[String] = ListBuffer.empty
  var nOfReconnect : Int = 0
  var nOfFailure : Int = 0

def InitActorStatic : ActorStatic = ActorStatic(false, false, 0, 0)

  def GetOrMake(XMPPClientID : String) : (String, ActorStatic) = mXMPPClientStatic.getOrElse(XMPPClientID, {
    val xmppStaticToMake = XMPPClientID -> InitActorStatic
    mXMPPClientStatic += xmppStaticToMake
    xmppStaticToMake
  }).asInstanceOf[(String, ActorStatic)]

  def XMPPClientConnected(clientID : String) : Unit = {
    if(!nOfSuccess.contains(clientID)) nOfSuccess += clientID
    if(nOfReconnecting.contains(clientID)) nOfReconnecting -= clientID
    if(nOfAuthenticated.contains(clientID)) nOfAuthenticated -= clientID
  }

  def XMPPLogInSuccess(clientID : String) : Unit = {
    if(!nOfSuccess.contains(clientID)) nOfSuccess += clientID
    if(!nOfAuthenticated.contains(clientID)) nOfAuthenticated += clientID
    if(nOfReconnecting.contains(clientID)) nOfReconnecting -= clientID
  }

  def XMPPReconnectSuccess(clientID : String) : Unit = {
    if(!nOfSuccess.contains(clientID)) nOfSuccess += clientID
    if(nOfReconnecting.contains(clientID)) nOfReconnecting -= clientID
    if(nOfAuthenticated.contains(clientID)) nOfAuthenticated -= clientID
    nOfReconnect += 1
  }

  def XMPPConnectionClisedOnError(clientID : String) : Unit = {
    if(nOfSuccess.contains(clientID)) nOfSuccess -= clientID
    if(!nOfReconnecting.contains(clientID)) nOfReconnecting += clientID
    if(nOfAuthenticated.contains(clientID)) nOfAuthenticated -= clientID
    nOfFailure += 1
  }

  def SlaveError() = {
    nOfFailure += 1
  }
}