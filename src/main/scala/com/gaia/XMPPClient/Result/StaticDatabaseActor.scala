package com.gaia.XMPPClient.Result

import akka.actor.{Props, ActorLogging, Actor}
import com.gaia.XMPPClient.ManagementMessage.{SlaveError}
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper.{ConnectionClosedOnError, ConnectionReconnectionSuccessful, ConnectionAuthenticated, ConnectionConnected}
import com.typesafe.config.ConfigFactory
import org.joda.time.DateTime
import slick.driver.MySQLDriver.api._
import slick.jdbc.meta.MTable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._

/**
 * Created by ktz on 15. 9. 9.
 */

case class RetryConnection()
class StaticDatabaseActor(InstanceID : String) extends StaticManager with Actor with ActorLogging{
  case class RequestFlushData()
  val slaveState = TableQuery[InstanceState]
  val connectionState = TableQuery[ConnectionState]
  val stateDatabase = Database.forConfig("mysql")
  val dataFlushInterval = ConfigFactory.load("application").getLong("test.interval.static-data-flush")
//  context.system.scheduler.schedule(dataFlushInterval millisecond, dataFlushInterval millisecond, self, RequestFlushData())
  stateDatabase.run(MTable.getTables).map{tables =>
    if(!tables.exists(_.name.name == "SlaveState")) stateDatabase.run(slaveState.schema.create)
  }
  stateDatabase.run(MTable.getTables).map{tables =>
    if(!tables.exists(_.name.name == "ConnectionState")) stateDatabase.run(connectionState.schema.create)
  }

  def receive = {
    case ConnectionConnected(connection) =>
      XMPPClientConnected(sender().path.name)
    case ConnectionAuthenticated(connection, resumed)=>
      XMPPLogInSuccess(sender().path.name)
    case ConnectionReconnectionSuccessful() =>
      XMPPReconnectSuccess(sender().path.name)
    case RetryConnection() =>
//      stateDatabase.run(connectionState += (InstanceID.toInt, "Retry", DateTime.now.toString("HH:mm:ss.SSS")))
    case ConnectionClosedOnError(e) =>
      XMPPConnectionClisedOnError(sender().path.name)
//      stateDatabase.run(connectionState += (InstanceID.toInt, "Disconnected", DateTime.now.toString("HH:mm:ss.SSS")))
    case SlaveError(e) =>
      SlaveError()
    case RequestFlushData() =>
      stateDatabase.run(slaveState.filter(_.InstanceID === InstanceID).result).onSuccess{
        case row : Seq[(String, Int, Int, Int, Int, Int, Long)] =>
          if(row.isEmpty) stateDatabase.run(slaveState += (InstanceID, nOfSuccess.size, nOfAuthenticated.size, nOfReconnecting.size, nOfReconnect, nOfFailure, DateTime.now.getMillis))
          else stateDatabase.run(slaveState.filter(_.InstanceID === InstanceID).map(p => (p.nOfConnect, p.nOfAuth, p.nOfReconnecting, p.nOfReconnected, p.nOfFailure, p.updatedTime)).update((nOfSuccess.size, nOfAuthenticated.size, nOfReconnecting.size, nOfReconnect, nOfFailure, DateTime.now.getMillis)))
      }
  }
}

object StaticDatabaseActor {
  def apply(clientID : String) = Props(new StaticDatabaseActor(clientID))
}