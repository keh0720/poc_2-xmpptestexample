package com.gaia.XMPPClient.XMPPActor

import java.io.{PrintWriter, StringWriter}
import java.nio.channels.AlreadyConnectedException

import akka.actor._
import com.gaia.XMPPClient.Result.RetryConnection
import com.gaia.XMPPClient._
import com.gaia.XMPPClient.ManagementMessage._
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper._
import org.jivesoftware.smack.{AbstractXMPPConnection, XMPPConnection}
import org.jivesoftware.smack.SmackException.{NoResponseException, NotConnectedException, ConnectionException}
import org.jivesoftware.smack.packet.{Presence}
import org.jivesoftware.smack.sasl.{SASLErrorException}
import org.jivesoftware.smackx.ping.PingManager
import scala.concurrent.Future
import scala.concurrent.duration._

/**
 * Created by ktz on 15. 7. 15.
 */

class XMPPClientActor(val StaticManager : ActorRef, val mServerName : String, val mHostAddress : String, mUserID : String, mUserPW : String, config : ActorConfig) extends XMPPClient with Actor with ActorLogging{
  import context.dispatcher
  case class RequestToConnectToServer() extends XMPPAction
  /**
   * Test Mode 전환을 위한 Mode 설정
   */

  val BecomeNormalMode : PartialFunction[TestMode, Unit] = {
    case NormalMode() =>
    case RandomStartMode() =>
  }

  val BecomeBruteMode : PartialFunction[TestMode, Unit] = {
    case BruteMode() =>
      context become BruteForce
    case ParallelStartMode() =>
      context become BruteForce
  }

  val ModeChanger = BecomeBruteMode orElse BecomeNormalMode
  ModeChanger(config.testMode)

  private var mConnection : AbstractXMPPConnection = null
  CleanConnection()
  private var mRequestedOperation : MsgEnvelope = null

  /**
   * Exception 처리하기 위한 Partial Function
   */
  val caseConnectionException : PartialFunction[Exception, Unit] = {
    case connectionException : ConnectionException =>
//      CleanConnection()
      log.debug(PrintError("connectionException - ", connectionException), connectionException)
      StaticManager ! SlaveError(connectionException)
  }

  val caseNotConnectedException : PartialFunction[Exception, Unit] = {
    case notConnectedException : NotConnectedException =>
      log.debug(PrintError("notConnectedException - ", notConnectedException), notConnectedException)
      StaticManager ! SlaveError(notConnectedException)
  }

  val caseException : PartialFunction[Exception, Unit] = {
    case exception : Exception  =>
//      CleanConnection()
      log.debug(PrintError("exception - ", exception), exception)
      StaticManager ! SlaveError(exception)
  }

  val caseSASLErrorException : PartialFunction[Exception, Unit] = {
    case saslErrorException : SASLErrorException =>
//      CleanConnection()
      log.debug(PrintError("saslErrorException - ", saslErrorException), saslErrorException)
      StaticManager ! SlaveError(saslErrorException)
  }

  val caseNoResponseException : PartialFunction[Exception, Unit] = {
    case noResponseException : NoResponseException =>
      log.debug(PrintError("noResponseException - ", noResponseException), noResponseException)
      StaticManager ! SlaveError(noResponseException)
  }

  val caseAlreadyConnected : PartialFunction[Exception, Unit] = {
    case alreadyConnected : AlreadyConnectedException =>
      self ! ConnectionConnected(mConnection)
      log.debug(PrintError("alreadyConnected - ", alreadyConnected), alreadyConnected)
      StaticManager ! SlaveError(alreadyConnected)
  }

  /**
   * XMPP 동작을 위한 Partial Function
   */

  val caseLogin : PartialFunction[(ActorRef, XMPPAction), Unit] = {
    case (from : ActorRef, action : RequestLogInAll) => LoginToServer(mConnection, mUserID, mUserPW) match {
      case Left(e) =>
        caseException(e)
        StartFromFirst(MsgEnvelope(self, RequestLogInAll()))
      case Right(b) =>
        GetFriendList(mConnection)
        ChangePresence(mConnection, Presence.Type.available)
    }
  }

  val caseCreateAccount : PartialFunction[(ActorRef, XMPPAction), Unit] = {
    case (from : ActorRef, action : RequestCreateAccountAll) => CreateAccount(mConnection, mUserID, mUserPW) match {
      case Left(e) =>
        caseException(e)
        StartFromFirst(MsgEnvelope(self, RequestCreateAccountAll()))
      case Right(b) =>
    }
  }

  def StartFromFirst(msg : MsgEnvelope) : Unit = {
//    CleanConnection()
    ModeChanger(config.testMode)
    StaticManager ! ConnectionClosedOnError(new Exception)
    mRequestedOperation = msg
    RetrySchedulerByTestMode()
  }

  /**
   * XMPP Connection Listener 처리를 위한 Partial Function
   */

//  val mChater = context.actorOf(Props(new ChaterActor(mConnection)))

  def GetMySequence(userID : String) : Int = userID.split("_")(1).toInt

  def ConnectToServerWithHandlingException() = {
    ConnectToServer(mConnection) match {
      case Left(connectionError) =>
        connectionError match {
          case alreadyConnected: AlreadyConnectedException =>
            caseAlreadyConnected(alreadyConnected)
          case exception: Exception =>
            (caseConnectionException orElse caseException)(exception)
            RetrySchedulerByTestMode()
        }
      case Right(b) =>
    }
  }

  def CleanConnection(): Unit = {
    if(mConnection != null && mConnection.isConnected) mConnection.disconnect(new Presence(Presence.Type.unavailable))
    mConnection = GetConnectionToServer(mServerName, mHostAddress, config.bosh)
    mConnection.addConnectionListener(XMPPConnectionListener(self))
    mConnection.setPacketReplyTimeout(config.packetReplyTimeOut)
  }

  def PrintError(kindOfError : String, exception : Exception) : String = {
    val errors = new StringWriter()
    exception.printStackTrace(new PrintWriter(errors))
    "\nResult Operation - Connection Exception : \n" + errors.toString
  }

  /**
   * Scheduler By TestMode
   */

  def RetrySchedulerByTestMode() = config.testMode match {
    case BruteMode() =>
      self ! RequestToConnectToServer()
    case _=>
      context.system.scheduler.scheduleOnce(RetryDuration, self, RequestToConnectToServer())
  }

  def RetryDuration : FiniteDuration = (scala.util.Random.nextInt(config.retryTimeOut - config.retryTimeMin) + config.retryTimeMin) millisecond

  def receive = {
    case MsgEnvelope(from, action) =>
      mRequestedOperation = MsgEnvelope(from, action)
      config.testMode match {
        case NormalMode() => context.system.scheduler.scheduleOnce((config.logIngSeqTime * GetMySequence(mUserID)) millisecond, self, RequestToConnectToServer())
        case RandomStartMode() => RetrySchedulerByTestMode()
      }
    case RequestToConnectToServer() =>
      ConnectToServerWithHandlingException()
    case ConnectionConnected(connection) =>
      context become Connected
      StaticManager ! ConnectionConnected(mConnection)
      (caseLogin orElse caseCreateAccount)(mRequestedOperation.from, mRequestedOperation.xmppAction)
  }

  def Connected : Receive = {
    case MsgEnvelope(from, action) =>
      action match {
        case RequestLogInAll() => caseLogin(from, action)
      }
    case ConnectionAuthenticated(connection : XMPPConnection, resumed : Boolean) =>
      context become LogIn
      PingManager.getInstanceFor(mConnection).setPingInterval(60)
      mRequestedOperation = null
      log.info("Connection Log In")
    case ConnectionClosedOnError(e) =>
      context become Disconnected
      StaticManager ! ConnectionClosedOnError(e)
      caseException(e)
      RetrySchedulerByTestMode()
    case ConnectionClosed() =>
      context become receive
  }

  def LogIn : Receive = {
    case MsgEnvelope(from, action) =>
    case ConnectionClosedOnError(e) =>
      context become Disconnected
      StaticManager ! ConnectionClosedOnError(e)
      caseException(e)
      mRequestedOperation = MsgEnvelope(self, RequestLogInAll())
      RetrySchedulerByTestMode()
      log.info("Connection Disconnected")
  }

  def Disconnected : Receive = {
    case RequestToConnectToServer() =>
      ConnectToServerWithHandlingException()
      StaticManager ! RetryConnection()
    case ConnectionConnected(connection) =>
      context become Connected
      StaticManager ! ConnectionReconnectionSuccessful()
      if(mRequestedOperation != null)
        (caseLogin orElse caseCreateAccount)(mRequestedOperation.from, mRequestedOperation.xmppAction)
    case MsgEnvelope(from, action) =>
      mRequestedOperation = MsgEnvelope(from, action)
  }

  def BruteForce : Receive = {
    case MsgEnvelope(from, action) =>
      mRequestedOperation = MsgEnvelope(sender(), action)
      ConnectToServerWithHandlingException()
      (caseLogin orElse caseCreateAccount)(mRequestedOperation.from, mRequestedOperation.xmppAction)
    case RequestToConnectToServer() =>
      ConnectToServerWithHandlingException()
      log.info("Connect Again")
      (caseLogin orElse caseCreateAccount)(mRequestedOperation.from, mRequestedOperation.xmppAction)
    case ConnectionConnected(connection) =>
      StaticManager ! ConnectionConnected(connection)
    case ConnectionAuthenticated(connection, resumed) =>
      StaticManager ! ConnectionAuthenticated(connection, resumed)
      PingManager.getInstanceFor(mConnection).setPingInterval(60)
    case ConnectionClosedOnError(e) =>
      log.info("Disconnected")
      StartFromFirst(mRequestedOperation)
  }
}

object XMPPClientActor {
  def apply(staticActor : ActorRef, mServerName : String, mHostAddress : String, mUserID : String, mUserPW : String, config : ActorConfig) : Props =
    Props(new XMPPClientActor(staticActor, mServerName, mHostAddress, mUserID, mUserPW, config))
}