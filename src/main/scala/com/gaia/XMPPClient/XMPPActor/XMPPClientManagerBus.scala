package com.gaia.XMPPClient.XMPPActor

import akka.actor.{ActorRef, ActorSystem}
import akka.event._
import com.gaia.XMPPClient.ManagementMessage.{XMPPAction, MsgEnvelope}

import scala.collection.mutable.ListBuffer

/**
 * Created by ktz on 15. 8. 22.
 */

class XMPPClientManagerBus extends EventBus with LookupClassification {

  type Classifier = String
  type Event = MsgEnvelope
  type Subscriber = ActorRef

  override protected def classify(event: Event): Classifier = "XMPPClient"

  override protected def mapSize: Int = 128

  override protected def compareSubscribers(a: ActorRef, b: ActorRef): Int = a compareTo b

  override protected def publish(event: Event, subscriber: ActorRef): Unit = {
    subscriber ! event
  }
}