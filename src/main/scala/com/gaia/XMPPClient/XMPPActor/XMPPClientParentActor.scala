package com.gaia.XMPPClient.XMPPActor

import akka.actor.Actor.Receive
import akka.actor.{ActorRef, Props, Actor}
import com.gaia.XMPPClient.ActorConfig

/**
 * Created by ktz on 15. 8. 31.
 */
class XMPPClientParentActor(val mStaticManager : ActorRef, val xmppClientBus : XMPPClientManagerBus, val nOfClientToCreate : Int, mySeq : Int, mServerAddress : String, mHostAddress : String, mID : String, mPW : String, config : ActorConfig) extends Actor {
  (0 until nOfClientToCreate) foreach { i =>
    val client = context.actorOf(XMPPClientActor(mStaticManager, mServerAddress, mHostAddress, s"${mID}_${i + nOfClientToCreate * mySeq}", mPW, config), s"Client_${mID}_${i + nOfClientToCreate * mySeq}")
    xmppClientBus.subscribe(client, "XMPPClient")
  }
    def receive = {
      case _ =>
    }
}

object XMPPClientParentActor {
  def apply(staticManager : ActorRef, xmppClientBus: XMPPClientManagerBus, nOfClient: Int, mySeq : Int, mServerAddress: String, mHostAddress : String, mID: String, mPW: String, config: ActorConfig): Props =
    Props(new XMPPClientParentActor(staticManager, xmppClientBus, nOfClient, mySeq, mServerAddress, mHostAddress : String, mID, mPW, config))
}