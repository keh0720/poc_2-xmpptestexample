package com.gaia.XMPPClient.XMPPActor

import akka.actor.{Actor, ActorLogging, ActorRef}
import com.gaia.XMPPClient.XMPPActor.XMPPWrapper.Chater
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.chat.{Chat, ChatMessageListener}
import org.jivesoftware.smack.packet.Message

import scala.collection.mutable.ListBuffer

/**
 * Created by ktz on 15. 7. 15.
 */
class ChaterActor(connection : XMPPConnection) extends Actor with ActorLogging with Chater {

  case class SendMessage(buddyID : String, message : String)
  case class MessageReceived(chat : Chat, message : Message)
  case class RequestAddChatReceiver(buddyID : String, receiver : ActorRef)

  import scala.collection.mutable.Map
  val mChatMap : Map[String, Chat] = Map.empty
  def receive = {
    case SendMessage(buddyID, message) =>
      val chat = GetOrCreateChat(mChatMap, buddyID) match {
        case Right(chat) => chat
        case Left(chat) =>
          mChatMap += buddyID -> chat
          chat
      }
      SendMessage(chat, message)
    case RequestAddChatReceiver(buddyID, receiver) =>
      val chat = GetOrCreateChat(mChatMap, buddyID) match {
        case Right(chat) => chat
        case Left(chat) =>
          mChatMap += buddyID -> chat
          chat
      }
      import scala.collection.JavaConversions._
      chat.getListeners.toList.head.asInstanceOf[MyChatMessageListener].RegisterMessageReceiver(receiver)
  }

  def GetOrCreateChat(chatMap : Map[String, Chat], buddyID : String) : Either[Chat, Chat] = chatMap.get(buddyID) match {
    case Some(chat) => Right(chat)
    case None => Left(CreateChat(connection, buddyID, new MyChatMessageListener))
  }

  class MyChatMessageListener extends ChatMessageListener{
    val mMessageReceiver : ListBuffer[ActorRef] = ListBuffer.empty

    def RegisterMessageReceiver(receiver : ActorRef) : Unit = {
      mMessageReceiver += receiver
    }

    override def processMessage(chat: Chat, message: Message): Unit = {
      if(mMessageReceiver.nonEmpty)
        mMessageReceiver.par.foreach{receiver =>
          receiver ! MessageReceived(chat, message)
        }
    }
  }
}
