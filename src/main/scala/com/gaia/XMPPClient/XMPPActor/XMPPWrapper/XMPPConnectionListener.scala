package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import akka.actor.Actor.Receive
import akka.actor.{Actor, ActorRef}
import org.jivesoftware.smack.tcp.XMPPTCPConnection
import org.jivesoftware.smack.{XMPPConnection, ConnectionListener}

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Future, Promise}

/**
 * Created by ktz on 15. 8. 17.
 */
trait ConnectionResult
case class ConnectionConnected(connection : XMPPConnection) extends ConnectionResult
case class ConnectionReconnectionFailed(e : Exception) extends ConnectionResult
case class ConnectionReconnectionSuccessful() extends ConnectionResult
case class ConnectionAuthenticated(connection: XMPPConnection, resumed: Boolean) extends ConnectionResult
case class ConnectionClosedOnError(e : Exception) extends ConnectionResult
case class ConnectionClosed() extends ConnectionResult
case class ConnectionReconnectingIn(second : Int) extends ConnectionResult

class XMPPConnectionListener(listenerActor : ActorRef) extends ConnectionListener{

  override def connected(connection: XMPPConnection): Unit = listenerActor ! ConnectionConnected(connection)

  override def reconnectionFailed(e: Exception): Unit = listenerActor ! ConnectionReconnectionFailed(e)

  override def reconnectionSuccessful(): Unit = listenerActor ! ConnectionReconnectionSuccessful()

  override def authenticated(connection: XMPPConnection, resumed: Boolean): Unit = listenerActor ! ConnectionAuthenticated(connection, resumed)

  override def connectionClosedOnError(e: Exception): Unit = listenerActor ! ConnectionClosedOnError(e)

  override def connectionClosed(): Unit = listenerActor ! ConnectionClosed()

  override def reconnectingIn(seconds: Int): Unit = listenerActor ! ConnectionReconnectingIn(seconds)
}

object XMPPConnectionListener{
  def apply(listenerActor : ActorRef) = new XMPPConnectionListener(listenerActor)
}