package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import org.jivesoftware.smack.SmackException.NotConnectedException
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.chat.{Chat, ChatManager, ChatMessageListener}


/**
 * Created by ktz on 15. 7. 15.
 */
trait Chater {

  def SendMessage(chat : Chat, message : String) : Either[Exception, Boolean] = {
    try{
      chat.sendMessage(message)
      Right(true)
    }
    catch {
      case e : NotConnectedException => Left(e)
    }
  }

  def CreateChat(connection : XMPPConnection, userID : String, messageListener: ChatMessageListener) : Chat =
    ChatManager.getInstanceFor(connection).createChat(userID, messageListener)
}