package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import java.io.IOException
import java.nio.channels.AlreadyConnectedException
import org.jivesoftware.smack.{XMPPConnection, SmackException, XMPPException, AbstractXMPPConnection}
import org.jivesoftware.smack.tcp.{XMPPTCPConnectionConfiguration, XMPPTCPConnection}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * Created by ktz on 15. 8. 17.
 */
trait XMPPTCPConnectionExtension extends XMPPTCPConnection {
//
//  def connectToServer() : Either[Exception, Future[ConnectionResult]] = {
//    val connectionListener = new XMPPConnectionListener
//    addConnectionListener(connectionListener)
//    try{
//      connect()
//      Right(connectionListener)
//    } catch {
//      case xmppException : XMPPException => Left(xmppException)
//      case smackException : SmackException => smackException match {
//        case e : AlreadyConnectedException => Right(connectionListener)
//        case otherSmackException : SmackException => Left(otherSmackException)
//      }
//      case ioException : IOException => Left(ioException)
//      case exception : Exception => Left(exception)
//    }
//  }
//
//  def DoAfterConnected(operation : XMPPConnection => Unit) : Future[Either[Exception, Boolean]] = {
//    connectToServer() match {
//      case Right(result) =>
//        result map {_ match {
//          case ConnectionConnected(connection) =>
//            try{
//              operation(connection)
//              Right(true)
//            }catch {
//              case xmppException : XMPPException => Left(xmppException)
//            }
//        }
//        }
//      case Left(e : Exception) =>
//        Future(Left(e))
//    }
//  }
}

object XMPPTCPConnectionExtension {
  def apply(jid : CharSequence, password : String) = new XMPPTCPConnection(jid, password) with XMPPTCPConnectionExtension
  def apply(config : XMPPTCPConnectionConfiguration) = new XMPPTCPConnection(config) with XMPPTCPConnectionExtension
  def apply(jid : CharSequence, password : String, serviceName : String) = new XMPPTCPConnection(jid, password, serviceName) with XMPPTCPConnectionExtension
}
