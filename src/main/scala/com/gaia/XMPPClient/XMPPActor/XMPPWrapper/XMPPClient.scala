package com.gaia.XMPPClient.XMPPActor.XMPPWrapper

import javax.net.ssl.SSLContext

import com.typesafe.config.ConfigFactory
import org.jivesoftware.smack.SmackException.{AlreadyLoggedInException}
import org.jivesoftware.smack.XMPPException.{StreamErrorException, XMPPErrorException}
import org.jivesoftware.smack._
import org.jivesoftware.smack.bosh.{BOSHConfiguration, XMPPBOSHConnection}
import org.jivesoftware.smack.packet.Presence
import org.jivesoftware.smack.packet.XMPPError.Condition
import org.jivesoftware.smack.roster.{Roster, RosterEntry}
import org.jivesoftware.smack.tcp.{XMPPTCPConnection, XMPPTCPConnectionConfiguration}
import org.jivesoftware.smack.util.TLSUtils
import org.jivesoftware.smackx.iqregister.AccountManager

import scala.collection.JavaConversions._

/**
 * Created by ktz on 15. 7. 15.
 */

trait XMPPClient {

  def GetConnectionToServer(serverName : String, hostName : String, bosh : Boolean) : AbstractXMPPConnection = {
    val config = if(bosh)CreateBoshConfig(serverName, hostName) else CreateConfig(serverName, hostName)
    if(bosh) new XMPPBOSHConnection(config.asInstanceOf[BOSHConfiguration]) else new XMPPTCPConnection(config.asInstanceOf[XMPPTCPConnectionConfiguration])
  }

  def ConnectToServer(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = {
    try{
      if(!mConnection.isConnected) mConnection.connect()
      Right(true)
    }catch {
      case e : Exception =>
        e.printStackTrace()
        Left(e)
    }
  }

  def LoginToServer(mConnection : AbstractXMPPConnection, userID : String, userPW : String) : Either[Exception, Boolean] = ConnectToServer(mConnection) match {
    case Right(b) =>
      try{
        mConnection.login(userID, userPW)
        Right(b)
      } catch {
        case alreadyConnectedException : AlreadyLoggedInException =>
          Right(false)
        case e : Exception => Left(e)
      }
    case Left(e) =>
      Left(e)
  }

  def LogOutToServer(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = {
    if (mConnection.isConnected) {
      try {
        mConnection.disconnect(new Presence(Presence.Type.unavailable, "", 1, Presence.Mode.away))
        Right(true)
      } catch {
        case e : Exception => Left(e)
      }
    }
    else{
      Right(false)
    }
  }

  def CreateConfig(serverName : String, hostName : String) : ConnectionConfiguration = {
    val builder = XMPPTCPConnectionConfiguration.builder()
    val configFactory = ConfigFactory.load("application")
    builder
      .setHost(serverName)
      .setServiceName(hostName)

    builder match {
      case myBuilder : XMPPTCPConnectionConfiguration.Builder =>
        myBuilder.setConnectTimeout(configFactory.getInt("test.interval.connection-timeout"))
    }

    val tls_enabled = configFactory.getBoolean("test.security.tls-enable")
    if(tls_enabled){
      builder.setSecurityMode(ConnectionConfiguration.SecurityMode.required)
      val sslContext = SSLContext.getInstance("TLS")
      sslContext.getServerSessionContext.setSessionTimeout(1)
      sslContext.getServerSessionContext.setSessionCacheSize(1)
      sslContext.init(null, Array(new DummyTrustManager), new java.security.SecureRandom())
      builder.setCustomSSLContext(sslContext)
      builder.setEnabledSSLCiphers(Array("TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA")).setEnabledSSLProtocols(Array("TLS"))
      TLSUtils.disableHostnameVerificationForTlsCertificicates(builder)
      TLSUtils.setSSLv3AndTLSOnly(builder)
    }

    ReconnectionManager.setEnabledPerDefault(false)
    builder.build()
  }

  def CreateBoshConfig(serverName : String, hostName: String): ConnectionConfiguration = {
    val builder = BOSHConfiguration.builder()
    val configFactory = ConfigFactory.load("application")
    builder
      .setHost(serverName)
      .setServiceName(hostName)
      .setPort(80)
      .setFile("/httpbind/httpbind").setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)

    val tls_enabled = configFactory.getBoolean("test.security.tls-enable")
    if(tls_enabled){
      builder.setSecurityMode(ConnectionConfiguration.SecurityMode.required)
    }

    ReconnectionManager.setEnabledPerDefault(false)
    builder.build()
  }

//  def CreateConfig(serverName : String) = CreateConfig(serverName, serverName)


  def ChangePresence(mConnection : AbstractXMPPConnection, presenceType : Presence.Type, statusMsg : String): Either[Exception, Boolean] ={
    val presence = new Presence(presenceType)
    if(statusMsg != "") presence.setStatus(statusMsg)
    try{
      mConnection.sendStanza(presence)
      Right(true)
    }
    catch {
      case e : Exception =>
        Left(e)
    }
  }

  def ChangePresence(mConnection : AbstractXMPPConnection, presenceType : Presence.Type) : Either[Exception, Boolean] =
    try{
      ChangePresence(mConnection, presenceType, "")
      Right(true)
    }catch {
      case e : Exception=>
        Left(e)
    }

  def GetFriendList(mConnection : AbstractXMPPConnection) : Either[Exception, List[RosterEntry]] = {
    try {
      if (!mConnection.isConnected) ConnectToServer(mConnection)
      Right(Roster.getInstanceFor(mConnection).getEntries.toList)
    }
    catch {
      case e : Exception =>
        Left(e)
    }
  }

  def AddFriend(connection : AbstractXMPPConnection, userName : String) : Either[Exception, Boolean] = {
    try{
      if(!connection.isConnected) ConnectToServer(connection)
      Roster.getInstanceFor(connection).createEntry(userName, userName.split("@")(0), null)
      Right(true)
    }catch {
      case e : Exception =>
        Left(e)
    }
  }

  def DeleteFriend(connection: AbstractXMPPConnection, userEntry : RosterEntry) : Either[Exception, Boolean] = {
    try{
      if(!connection.isConnected) ConnectToServer(connection)
      Roster.getInstanceFor(connection).removeEntry(userEntry)
      Right(true)
    }catch {
      case e : Exception =>
        Left(e)
    }
  }


  def CreateAccount(mConnection : AbstractXMPPConnection, userID : String, userPW : String) : Either[Exception, Boolean] = {
    try{
      AccountManager.getInstance(mConnection).createAccount(userID, userPW)
      Right(true)
    } catch {
      case xmppErrorException : XMPPErrorException =>
        xmppErrorException.getXMPPError.getCondition match {
          case Condition.conflict =>
            Right(false)
          case _=>
            Left(xmppErrorException)
        }
      case e : Exception =>
        Left(e)
    }
  }


  def DeleteAccount(mConnection : AbstractXMPPConnection) : Either[Exception, Boolean] = {
    try{
      if(!mConnection.isConnected) ConnectToServer(mConnection)
      AccountManager.getInstance(mConnection).deleteAccount()
      Right(true)
    }catch {
      case streamErrorException : StreamErrorException =>
        Left(streamErrorException)
      case e : Exception =>
        Left(e)
    }
  }
}