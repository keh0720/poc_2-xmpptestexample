package com.gaia.XMPPClient

import akka.actor.{Props, ActorSystem}
import com.typesafe.config.ConfigFactory

/**
 * Created by ktz on 15. 7. 17.
 */
object Main extends App {
  val system = ActorSystem("testSystem")
  val client = system.actorOf(Props(new ActorClient(s"masterSystem@${ConfigFactory.load("application").getString("akka.master")}", "MasterActor")), "ActorClient")
}

