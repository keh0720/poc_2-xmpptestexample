package com.gaia.XMPPClient

/**
 * Created by ktz on 15. 8. 4.
 */
case class IllegalIPAddressException(msg : String) extends IllegalArgumentException(msg)
