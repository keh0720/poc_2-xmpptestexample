name := "XMPPServerLoadTest"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {
  val Akkav = "2.3.9"
  val SmackV = "4.1.3"
  val kamonVersion = "0.3.5"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % Akkav,
    "com.typesafe.akka" %% "akka-remote" % Akkav,
    "com.typesafe.akka" %% "akka-slf4j" % Akkav,
    "com.github.nscala-time" %% "nscala-time" % "2.0.0",
    "org.igniterealtime.smack" % "smack-tcp" % SmackV,
    "org.igniterealtime.smack" % "smack-bosh" % SmackV,
    "org.igniterealtime.smack" % "smack-java7" % SmackV,
    "org.igniterealtime.smack" % "smack-im" % SmackV,
    "org.igniterealtime.smack" % "smack-extensions" % SmackV,
    "com.typesafe.slick" %% "slick" % "3.0.2",
    "mysql" % "mysql-connector-java" % "5.1.6",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "org.scalaz" %% "scalaz-core" % "7.1.3",
    "com.zaxxer" % "HikariCP" % "2.4.1",
    "com.github.nscala-time" %% "nscala-time" % "2.2.0"
  )
}